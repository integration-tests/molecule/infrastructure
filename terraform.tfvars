
# Size in bytes (20 GB)
# size = 21474836480
# Size in bytes (10 GB)
# size = 10737418240
# Size in bytes (8 GB)
# size = 8589934592
#         6442450944

servers = {

    database = {
        memory    = 2048
        vcpu      = 1
        disk_size = "6442450944"
        octetIP   = 10
        hostname  = "database-1"
    }
}
