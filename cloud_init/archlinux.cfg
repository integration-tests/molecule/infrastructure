#cloud-config
# vim: syntax=yaml

# Set hostname to "external.fqdn.me" instead of "myhost"
fqdn: ${hostname}.docker.local
hostname: ${hostname}
prefer_fqdn_over_hostname: true
create_hostname_file: true

keyboard:
  layout: de
  model: pc105
  variant: nodeadkeys
  options: compose:rwin

locale: de_DE

ssh_pwauth: true

# add non privileged user for later ansible runs
users:
  - name: sysadm
    gecos: SysAdm
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPHUuPrbu9kSKL1u0gfViSa/P7FkZPk8hYbv7hegpTcY bodo@boone-schulz.de
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG3/Vt8RKhgN99v6FJ1T13so0Bc6Tq1FkoTC1YAQ4zLK bodo.schulz@bitgrip.de
    sudo: ALL=(ALL) NOPASSWD:ALL
    shell: /bin/bash
    groups: admin

  - name: ansible
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPHUuPrbu9kSKL1u0gfViSa/P7FkZPk8hYbv7hegpTcY bodo@boone-schulz.de
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG3/Vt8RKhgN99v6FJ1T13so0Bc6Tq1FkoTC1YAQ4zLK bodo.schulz@bitgrip.de
    sudo: ALL=(ALL) NOPASSWD:ALL
    shell: /bin/bash
    groups: users,admin,wheel,sudo

# By default, the fingerprints of the authorized keys for the users
# cloud-init adds are printed to the console. Setting
# no_ssh_fingerprints to true suppresses this output.
no_ssh_fingerprints: false

# By default, (most) ssh host keys are printed to the console. Setting
# emit_keys_to_console to false suppresses this output.
ssh:
  emit_keys_to_console: false

# root user should be available
disable_root: false

# change some passwords
#
# - create password:
#   makepasswd --minchars 20 --maxchars 20
# - hash the generated passwort with openssl:
#   (Note: passing -1 will generate an MD5 password, -5 a SHA256 and -6 SHA512 (recommened))
#   openssl passwd -6 -salt fdfngmklndfgbl   PASSWORD
chpasswd:
  expire: False
  users:
    - name: root
      password: "$6$fdfngmklndfgbl$fhdX3uCUIwHdeiGk4/tbmR50q5UCGdHPckqn0HX9vBLEwc2hh8CWAE4ryawvQF.WDKb60O.uJCdPhTTxzxSUX/"
    - name: sysadm
      password: "$6$fdfngmklndfgbl$fhdX3uCUIwHdeiGk4/tbmR50q5UCGdHPckqn0HX9vBLEwc2hh8CWAE4ryawvQF.WDKb60O.uJCdPhTTxzxSUX/"
    - name: ansible
      password: "$6$fdfngmklndfgbl$H6Z6MdP23KTogsA01.pjmVueKqkeL/vGLM.KAhDC14u6gTwOfCGA9PQBQigiRtywiPhMC5MDx6b56WGPagUIU."

# https://cloudinit.readthedocs.io/en/latest/topics/examples.html#grow-partitions
growpart:
  mode: auto
  devices: ['/']

# # Run apt or yum upgrade
# # https://cloudinit.readthedocs.io/en/latest/topics/examples.html#update-apt-database-on-first-boot
package_update: true
# # https://cloudinit.readthedocs.io/en/latest/topics/examples.html#run-apt-or-yum-upgrade
package_upgrade: true

# Install arbitrary packages
# https://cloudinit.readthedocs.io/en/latest/topics/examples.html#install-arbitrary-packages
packages:
  - bash-completion
  - nano
  - less
  - syslog-ng
  - qemu-guest-agent
  - htop
  - tree

runcmd:
  - rm -f /etc/resolv.conf
  - echo "nameserver 141.1.1.1" | sudo tee /etc/resolv.conf
  - echo "net.ipv6.conf.all.disable_ipv6 = 1" | sudo tee /etc/sysctl.d/10-disable-ipv6.conf
  - systemctl enable syslog-ng@default

# test of writing content
write_files:
  - path: "/etc/vconsole.conf"
    permissions: '0644'
    content: |
      KEYMAP=de-latin1-nodeadkeys
      FONT="ter-v32b"

# written to /var/log/cloud-init-output.log
final_message: "The system is finall up, after $UPTIME seconds"

power_state:
  mode: reboot
  message: rebooting
  timeout: 10
  condition: True
