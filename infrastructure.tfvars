
# Size in bytes (20 GB)
# size = 21474836480
# Size in bytes (10 GB)
# size = 10737418240
# Size in bytes (8 GB)
# size = 8589934592
# Size in bytes (4 GB)
# size = 4294967296

servers = {
  "molecule" = {
    "memory"    = 6144
    "vcpu"      = 3
    "disk_size" = "21474836480"
    octetIP     = 10
    "hostname"  = "molecule"
  }
}
