
resource "libvirt_network" "vm_network" {

  name      = "docker_network"
  domain    = "docker.local"
  addresses = ["192.168.110.0/24"]

  dns {
    enabled    = true
    local_only = true
  }
}
