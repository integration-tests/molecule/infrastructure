THISDIR := $(notdir $(CURDIR))
PROJECT := $(THISDIR)

plan:
	terraform plan -var-file=$(PROJECT).tfvars

apply:
	terraform apply -auto-approve -var-file=$(PROJECT).tfvars

init:
	terraform init

show:
	terraform show

## recreate terraform resources
rebuild: destroy apply

destroy:
	terraform destroy -auto-approve

metadata:
	terraform refresh

## validate syntax of cloud_init
validate-cloud-config:
	cloud-init devel schema --config-file cloud_init.cfg
