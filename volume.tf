

resource "libvirt_volume" "arch_image" {
  name     = "archlinux-image"
  pool     = "pool"
  source   = "/var/lib/libvirt/pool/arch-linux-x86_64.qcow2"
}

resource "libvirt_volume" "diskimage" {

  for_each       = var.servers

  pool           = "default"
  name           = "${each.key}.qcow2"
  # base_volume_id = "libvirt_volume.${lookup(var.servers[each.key], "base_image", "debian12")}_image.id"
  base_volume_id = libvirt_volume.arch_image.id
  size           = lookup(var.servers[each.key], "disk_size", "") == "" ? "0" : var.servers[each.key].disk_size
}
